package com.expedia;

import com.expedia.base.TestUtilities;
import com.google.common.collect.Ordering;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.ResultsPage;

import java.util.List;

public class SearchTests extends TestUtilities {

    @Test(dataProvider = "searchParameters")
    public void sortingPricesTest(Object[] searchParameters) {

        MainPage mainPage = new MainPage(driver);
        ResultsPage resultsPage = mainPage.openPage()
                .setSearchParameters(searchParameters)
                .search();

        resultsPage.sortByHighestPrice();
        List<Integer> expectedPricesList = resultsPage.getPrices();

        Assert.assertTrue(Ordering.natural().reverse().isOrdered(expectedPricesList), "The list of prices is not in descending order");
    }
}
