package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePageObject {
    protected WebDriver driver;

    public BasePageObject(WebDriver driver) {
        this.driver = driver;
    }

    protected void openURL(String url) {
        driver.get(url);
    }

    protected WebElement find(By locator) {
        return driver.findElement(locator);
    }

    protected List<WebElement> findAll(By locator) {
        return driver.findElements(locator);
    }

    protected void click(By locator) {
        waitForElement(locator);
        find(locator).click();
    }

    protected void fill(String text, By locator) {
        waitForElement(locator);
        find(locator).sendKeys(text);
    }

    /**
     * There was a bug with `Return Date` input, have to set attribute value using JS
     */
    protected void fillReturnDate(String date, By locator) {
        setAttributeValue(find(locator), date);
        find(By.cssSelector(".datepicker-close-btn")).click();
    }

    public void setAttributeValue(WebElement element, String value) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String scriptSetAttrValue = "arguments[0].setAttribute(arguments[1],arguments[2])";

        js.executeScript(scriptSetAttrValue, element, "value", value);
    }

    protected void waitForElement(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }


}
