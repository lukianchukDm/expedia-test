package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class ResultsPage extends BasePageObject {

    private By sortDropdownLocator = By.id("sortDropdown");
    private By pricesLocator = By.cssSelector("[data-test-id='listing-price-dollars']");
    private By resetSorting = By.className("sort-filter-clear-button");

    public ResultsPage(WebDriver driver) {
        super(driver);
    }

    public void sortByHighestPrice() {
        waitForElement(pricesLocator);

        String valueBefore = find(pricesLocator).getText();

        WebElement sortDropdown = find(sortDropdownLocator);
        Select dropdown = new Select(sortDropdown);

        dropdown.selectByVisibleText("Price (Highest)");
        waitForSorting(pricesLocator, valueBefore);
    }

    protected void waitForSorting(By locator, String valueBefore) {
        String valueAfter = null;
        System.out.print("Sorting the list");
        do {
            System.out.print(".");
            try {
                valueAfter = find(locator).getText();
            } catch (StaleElementReferenceException e) {
            }
        } while (valueBefore.equals(valueAfter));
    }

    public List<Integer> getPrices() {
        System.out.println("\nReading prices");
        waitForElement(pricesLocator);

        List<WebElement> prices = findAll(pricesLocator);
        List<Integer> results = new ArrayList<>();

        for (WebElement price : prices) {
            String rawPrice = price.getText();
            String formattedPrice = rawPrice.replace("$", "").replace(",", "");
            results.add(Integer.parseInt(formattedPrice));
        }
        return results;
    }
}
