package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class MainPage extends BasePageObject {

    private String url = "https://www.expedia.com/";

    private By flightButton = By.id("tab-flight-tab-hp");
    private By flyingFromInput = By.id("flight-origin-hp-flight");
    private By flyingToInput = By.id("flight-destination-hp-flight");
    private By departingDateInput = By.id("flight-departing-hp-flight");
    private By returningDateInput = By.id("flight-returning-hp-flight");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public MainPage openPage() {
        System.out.println("Opening main page");
        openURL(url);

        waitForElement(flightButton);
        return this;
    }

    public MainPage setSearchParameters(Object[] searchParameters) {
        System.out.println("Filling search parameters");
        click(flightButton);

        String flyingFrom = searchParameters[0].toString();
        String flyingTo = searchParameters[1].toString();
        String departingDate = searchParameters[2].toString();
        String returningDate = searchParameters[3].toString();

        fill(flyingFrom, flyingFromInput);
        fill(flyingTo, flyingToInput);
        fill(departingDate, departingDateInput);
        fillReturnDate(returningDate, returningDateInput);

        return this;
    }

    public ResultsPage search() {
        System.out.println("Searching...");
        find(returningDateInput).sendKeys(Keys.ENTER);
        return (new ResultsPage(driver));
    }

}
