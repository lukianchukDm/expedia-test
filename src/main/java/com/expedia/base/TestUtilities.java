package com.expedia.base;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class TestUtilities extends BaseTest {

    @DataProvider(name = "searchParameters")
    protected static Object[][] searchParameters() {
        String flyingFrom = "Kyiv";
        String flyingTo = "Hermosillo";
        String departureDate = getDates()[0];
        String returningDate = getDates()[1];

        return new Object[][]{
                {flyingFrom, flyingTo, departureDate, returningDate}
        };
    }

    private static String[] getDates() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        String departureDate = formatter.format(LocalDateTime.now().plusDays(1));
        String returningDate = formatter.format(LocalDateTime.now().plusDays(14));

        return (new String[]{departureDate, returningDate});
    }

    protected void takeScreenshot(String fileName) {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String path = System.getProperty("user.dir")
                + File.separator + "test-output"
                + File.separator + "screenshots"
                + File.separator + getTodaysDate()
                + File.separator + testSuiteName
                + File.separator + testName
                + File.separator + testMethodName
                + File.separator + getSystemTime()
                + " " + fileName + ".png";
        try {
            FileUtils.copyFile(scrFile, new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getTodaysDate() {
        return (new SimpleDateFormat("yyyyMMdd").format(new Date()));
    }

    private String getSystemTime() {
        return (new SimpleDateFormat("HHmmssSSS").format(new Date()));
    }

}
